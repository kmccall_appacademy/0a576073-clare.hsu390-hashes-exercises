# EASY

# Define a method that, given a sentence, returns a hash of each of the words as
# keys with their lengths as values. Assume the argument lacks punctuation.
def word_lengths(str)
  new_hash = { }
  str.split.each do |el|
    new_hash[el] = el.length
  end
  new_hash
end

# Define a method that, given a hash with integers as values, returns the key
# with the largest value.
def greatest_key_by_val(hash)
  hash.sort_by {|k,v| v}.last.first
end

# Define a method that accepts two hashes as arguments: an older inventory and a
# newer one. The method should update keys in the older inventory with values
# from the newer one as well as add new key-value pairs to the older inventory.
# The method should return the older inventory as a result. march = {rubies: 10,
# emeralds: 14, diamonds: 2} april = {emeralds: 27, moonstones: 5}
# update_inventory(march, april) => {rubies: 10, emeralds: 27, diamonds: 2,
# moonstones: 5}
def update_inventory(older, newer)
  newer.each do |k,v|
    older[k] = v
  end
  older
end

# Define a method that, given a word, returns a hash with the letters in the
# word as keys and the frequencies of the letters as values.
def letter_counts(word)
  new_hash = Hash.new(0)
  word.each_char do |ch|
    new_hash[ch] += 1
  end
  new_hash
end

# MEDIUM

# Define a method that, given an array, returns that array without duplicates.
# Use a hash! Don't use the uniq method.
def my_uniq(arr)
  new_hash = {}
  arr.each do |el|
    new_hash[el] = true
  end
  new_hash.keys
end

# Define a method that, given an array of numbers, returns a hash with "even"
# and "odd" as keys and the frequency of each parity as values.
def evens_and_odds(numbers)
  new_hash = {:even => 0, :odd => 0}
  numbers.each do |el|
    new_hash[:even] += 1 if el.even?
    new_hash[:odd] += 1 if el.odd?
  end
  new_hash
end

# Define a method that, given a string, returns the most common vowel. If
# there's a tie, return the vowel that occurs earlier in the alphabet. Assume
# all letters are lower case.
def most_common_vowel(string)
  vowels = "aeiou"
  new_hash = Hash.new(0)
  string.each_char do |ch|
    new_hash[ch] += 1 if vowels.include?(ch)
  end
  most_common = new_hash.sort_by {|k,v| v}.last.last
  new_hash.select {|k,v| v == most_common}.sort.first.first
end

# HARD

# Define a method that, given a hash with keys as student names and values as
# their birthday months (numerically, e.g., 1 corresponds to January), returns
# every combination of students whose birthdays fall in the second half of the
# year (months 7-12). students_with_birthdays = { "Asher" => 6, "Bertie" => 11,
# "Dottie" => 8, "Warren" => 9 }
# fall_and_winter_birthdays(students_with_birthdays) => [ ["Bertie", "Dottie"],
# ["Bertie", "Warren"], ["Dottie", "Warren"] ]
def fall_and_winter_birthdays(students)
  second_half = students.select {|k,v| v > 6}
  new_arr = []
  second_half.keys.each_with_index do |el, idx|
    (idx+1..second_half.length-1).each do |idx2|
      new_arr << [el, second_half.keys[idx2]]
    end
  end
  new_arr
end

# Define a method that, given an array of specimens, returns the biodiversity
# index as defined by the following formula: number_of_species**2 *
# smallest_population_size / largest_population_size biodiversity_index(["cat",
# "cat", "cat"]) => 1 biodiversity_index(["cat", "leopard-spotted ferret",
# "dog"]) => 9
def biodiversity_index(specimens)
  number_of_species = specimens.uniq.length
  new_hash = Hash.new(0)
  specimens.each do |el|
    new_hash[el] += 1
  end
  smallest = new_hash.sort_by {|k,v| v}.first.last
  largest = new_hash.sort_by {|k,v| v}.last.last
  number_of_species**2 * smallest/largest
end

# Define a method that, given the string of a respectable business sign, returns
# a boolean indicating whether pranksters can make a given vandalized string
# using the available letters. Ignore capitalization and punctuation.
# can_tweak_sign("We're having a yellow ferret sale for a good cause over at the
# pet shop!", "Leopard ferrets forever yo") => true
def can_tweak_sign?(normal_sign, vandalized_sign)
  normal_count = character_count(normal_sign)
  vandalized_count = character_count(vandalized_sign)
  normal_count.all? do |k,v|
    vandalized_count[k] <= v
  end
end

def character_count(str)
  new_hash = Hash.new(0)
  str.downcase.each_char do |ch|
    new_hash[ch] += 1
  end
  new_hash
end
